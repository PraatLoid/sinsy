/*
 * SynthSG -- A wrapper library around the espeak-sg binary based on mbrowrap.c
 *
 * Copyright (C) 2005 to 2013 by Jonathan Duddington
 * Copyright (C) 2010 by Nicolas Pitre <nico@fluxnic.net>
 * Copyright (C) 2013-2016 Reece H. Dunn
 * Copyright (C) 2020 Tobias Platen
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
 
#ifndef SINSY_SYNTHSG_H_
#define SINSY_SYNTHSG_H_

#include <string>

namespace sinsy
{

class SynthSG
{
  int pid;
  std::string voice;
  std::string f0;
  std::string lyric;
  std::string rate;
  std::string filename;
  std::string opt1;
  std::string opt2;
public:
  SynthSG(std::string voice,
          std::string f0,
          std::string lyric,
          std::string rate,
          std::string filename,
          std::string opt1="",
          std::string opt2=""
          );
  void start();
  bool wait(int &status);
};

}

#endif


