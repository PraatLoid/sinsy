#ifndef OTO_H
#define OTO_H

#include <string>
#include <vector>

namespace espeaksg {

struct SGParam
{
    std::string key;
    std::vector<std::string> values;
};

class Oto
{
    std::string _path;
    std::string _def_lang;
    std::vector<SGParam*> _params;
    SGParam* find(std::string const& key);
    SGParam* scan(std::string const& line);
public:
    Oto(std::string const& path,std::string const& def_lang);
    std::string getVoice(std::string const& key);
    std::string getRate(std::string const& key);
    ~Oto();
    void write();

};
}

#endif // OTO_H
