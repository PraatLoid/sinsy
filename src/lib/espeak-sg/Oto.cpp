// UTAU: oto=LeftBlank,FixedLength,RightBlank,PreUtterance,VoiceOverlap
// ESPEAK: key=voice,rate

#include "Oto.h"
#include <algorithm>
#include <fstream>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>


namespace espeaksg {



    Oto::Oto(std::string const& path,std::string const& def_lang) : _path(path), _def_lang(def_lang)
    {
        std::ifstream file(path);
        if (file.is_open()) {
            std::string line;
            while (std::getline(file, line)) {
                _params.push_back(scan(line));
            }
            file.close();
        }
    }

    Oto::~Oto()
    {
        for(auto p: _params)
            delete p;
    }

    SGParam* Oto::find(std::string const& key)
    {
        for(auto p: _params)
        {
            if(p->key == key) return p;
        }
        auto p = new SGParam;
        p->key = key;
        p->values.push_back(_def_lang); //FIXME do not hardcode
        p->values.push_back("0"); 
        _params.push_back(p);
        return p;
    }

    SGParam* Oto::scan(const std::string &line)
    {
        std::string::size_type index = line.find( "=" );

        SGParam* p = new SGParam();
        p->key = line.substr( 0, index );
        std::string next = line.substr( index+1 );
        boost::split(p->values, next, boost::is_any_of(","));
        return p;
    }

    std::string Oto::getVoice(std::string const& key)
    {
        auto p = find(key);
        return p->values[0];
    }

    std::string Oto::getRate(std::string const& key)
    {
        auto p = find(key);
        return p->values[1];
    }

    void Oto::write()
    {

        std::ofstream ofs (_path, std::ofstream::out);

        for(auto p: _params)
        {
             ofs << p->key << "=";
             for(uint i=0;i<p->values.size(); i++)
             {
                   if(i==p->values.size()-1)
                       ofs << p->values[i] << std::endl;
                   else
                       ofs << p->values[i] << ",";
             }
        }

        ofs.close();

    }


}




