#include "LyricTimeline.h"
#include "util_log.h"
#include <boost/algorithm/string.hpp>

namespace sinsy {

void LyricTimeline::addLyric(std::string lyric,float start,float length,int midinote,std::string dynamics)
{
    boost::algorithm::to_lower(lyric);
    if(lyric.length()==0 || lyric=="_" || lyric=="-")
    {
         if(error || points.size()==0) {
             ERR_MSG("cannot extend rest");
             return;
         }
        
         lyricPoint* l= points.back();
         
         if(l->times.back() == start)
         {
             l->times.push_back(start+length);
             l->midinotes.push_back(midinote);
             LOG_MSG("extend");
         }
         else
            error = true;
         
    }
    else
    {
        lyricPoint* l=new lyricPoint();
        l->lyric = lyric;
        l->times.push_back(start);
        l->times.push_back(start+length);
        l->midinotes.push_back(midinote);
        l->dynamics = dynamics;
        points.push_back(l);
        LOG_MSG("add lyric "<<lyric);
        error=false;
    }
        
}
void LyricTimeline::fix(){}
bool LyricTimeline::isValid(){return true;}

void LyricTimeline::outputPho(IUnisyn* synth)
{ 
	printf("calling emit in a loop\n");
    for (int i = 0; i < points.size(); i++) 
    {
            auto p = points[i];
            if (i < points.size() - 1)
                emit(p,points[i + 1],synth);
            else
                emit(p,nullptr,synth);
    }
}

int LyricTimeline::getEventCount()
{
    return points.size();
}

std::string LyricTimeline::getLyric(int index)
{
    return points[index]->lyric;
}

int LyricTimeline::getPitch(int index,bool maxElement)
{
    if(maxElement)
    {
        return *max_element(points[index]->midinotes.begin(),points[index]->midinotes.end());
    }
    else
    {
        return *min_element(points[index]->midinotes.begin(),points[index]->midinotes.end());
    }
}

void LyricTimeline::setParam(int index,std::string key,std::string value)
{
      points[index]->params[key]=value;
}
void LyricTimeline::setParam(int index,std::string key,float value)
{
    points[index]->paramsf[key]=value;
}

float LyricTimeline::getStart(int index)
{
	if(points[index]->times.size()==0) return 0;
	return points[index]->times[0];
}

float LyricTimeline::getEnd(int index)
{
	int len = points[index]->times.size();
	if(len<2) return 0;
	return points[index]->times[len-1];
}

//end namespace
}
