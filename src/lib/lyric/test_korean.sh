# [THE UNI+] Blooming – Always

espeak -v ko --ipa "오늘 해가 지면 다가올 내 밤하늘은 "
espeak -v ko --ipa "널 만날 생각으로 새빨갛게 물들고 "
espeak -v ko --ipa "내일 너를 보면 어떤 말을 해야 할까 "

#oneul haega jimyeon  dagaol   nae bamhaneureun
#ˈonɯɫ hˈɛqɐ tɕˈimjʌn dˈɐqɐˌoɫ nˈɛ pˈɐmhɐnˌɯɾɯn

#nˈʌɫ mˈɐnnɐɫ sˈɛŋqɐqˌɯɾo  sɛp-ˈɐɫqɐt-khˌe mˈuɫdɯɫqˌo
#neol mannal  saenggageuro saeppalgake     muldeulgo

#nɛˈiɫ nʌɾˈɯɫ  pˈomjʌn ʌt-ˈʌn  mɐɾˈɯɫ hˈɛjɐ hˈɐɫq-ɐ
#naeil neoreul bomyeon eotteon mareul haeya halkka

#https://www.koreanwikiproject.com/wiki/index.php?title=IPA
