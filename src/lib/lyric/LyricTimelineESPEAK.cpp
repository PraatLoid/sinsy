#include "LyricTimelineESPEAK.h"
#include "util_log.h"
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/lexical_cast.hpp>
#include <fstream>

std::string pho(int i)
{
    char ret[32];
    sprintf(ret,"PHO[%i]",i);
    return ret;
}

// stretch vowel only to fill note, enough room for others
// enough room, no stretch needed anywhere, insert REST
// squish vowel, keep C and O same, insert REST

namespace sinsy {

ILyricTimeline* newTimelineESPEAK() { return new LyricTimelineESPEAK(); }

LyricTimelineESPEAK::LyricTimelineESPEAK()
{
    //TODO
}

//unify preutter and overlap ??
static float oto_preutter(lyricPoint* p)
{
    if(p->paramsf.count("PreUtterance"))
        return p->paramsf["PreUtterance"];
    return 0;
}
static float oto_overlap(lyricPoint* p)
{
    if(p->paramsf.count("VoiceOverlap"))
        return p->paramsf["VoiceOverlap"];
    return 0;
}

void LyricTimelineESPEAK::fix()
{

}

static void doDefault(lyricPoint* p,bool mbrolasing, bool next)
{
  float length_vowel = 0;
  float length_coda = 0;
  float length_onset = 0;
  int vowel_count = 0;
  
  if(p->pho_times.size()!=0) return;
  
  for(int i=0;i<256;i++)
  {
       std::string key = pho(i);
       std::string value = p->params[key];
       //printf("doDefault key=<%s> value=<%s>\n",key.c_str(),value.c_str());
       if(value=="") break;
       
       std::vector<std::string> spl;
       boost::split(spl, value, boost::is_any_of("\t "),
                   boost::token_compress_on);
                   
       float length=0;
       
       std::string code;
       std::string type;
  
	   if(mbrolasing) // this is wrong -- called twice
	   {
		    abort(); //not implemented yet
		    if(next==false)
		    {
			//TODO mbrolasing format
			// j LIQUID 0.029025 0.014512
			code = spl[0];
			type = spl[1];
			length = boost::lexical_cast<float>(spl[2]);
			//float lm = boost::lexical_cast<float>(spl[3]); TODO save this
			
			if(code[0]!='_')
			{
				p->pho_lengths.push_back(length);
				p->pho_codes.push_back(code);
				p->pho_types.push_back(type);
			}
			}
			
			//DO we have pho_times?
	   }
	   else
	   {
		 
			code = spl[2];
			type = spl[3];
			
			//printf("code=<%s> type=<%s>\n",code.c_str(),type.c_str());
       
			if(type=="PAUSE" && length_vowel>0) break;
			
			float a = boost::lexical_cast<float>(spl[0]);
			float b = boost::lexical_cast<float>(spl[1]);
			length = (b-a);
			p->pho_times.push_back(a);
			p->pho_lengths.push_back(length);
			p->pho_codes.push_back(code);
			p->pho_types.push_back(type);
		}
       
		if(type=="VOWEL")
		{
            length_vowel+=length;
            vowel_count++;
		}
        else
        {
			if(length_vowel)
			{
				length_coda += length; 
			}
			else
			{
				length_onset += length;
			}
       }
       
       
    
       
  }
  //LOG_MSG("len o="<<length_onset<<" v="<<length_vowel<<" c="<<length_coda);
  p->paramsf["PreUtterance"] = length_onset;
  p->paramsf["DefVowel"] = length_vowel;
  p->paramsf["DefCoda"] = length_coda;
  p->paramsf["VowelCount"] = (float)vowel_count;
  
  if(!mbrolasing)
  {
	uint sz = p->pho_times.size();
	assert(sz>=1);
	p->pho_times.push_back(p->pho_times[sz-1] + p->pho_lengths[sz-1]); //last one
  }
  
}

void LyricTimelineESPEAK::emit(lyricPoint* cur,lyricPoint* next,IUnisyn* synth)
{
 
  doDefault(cur,mbrolasing, false);
  if(next) doDefault(next,mbrolasing, true);
  
  float n_start = cur->times[0];
  float n_end =  cur->times.back();

  if (next) {
     n_end -= oto_preutter(next);
     n_end += oto_overlap(next);
  }
  
  n_start -= oto_preutter(cur);
  float n_len = n_end - n_start;
  
  //TODO: handle diphone voices (festival/mbrola)
  float def_length = cur->paramsf["PreUtterance"]+cur->paramsf["DefVowel"]+cur->paramsf["DefCoda"];
  printf("PreUtterance %f DefVowel %f DefCoda %f\n",cur->paramsf["PreUtterance"],cur->paramsf["DefVowel"],cur->paramsf["DefCoda"]);
  float vowel_count = cur->paramsf["VowelCount"];
  //assert(def_length>0);
  
  //THIS data looks bad
  float delta = n_len - def_length;
  //printf("n_len=%f def_len=%f\n",n_len,def_length);
  
  // change length of phonemes to fit not duration
  std::vector<float> out_lengths;
  if(delta>0)
  {
	  
      for(int i=0;i<cur->pho_lengths.size();i++)
      {
          float length = cur->pho_lengths[i];
          printf("extend %f\n",length);
          if(cur->pho_types[i]=="VOWEL")
          {
              length += delta/vowel_count;
          }
          out_lengths.push_back(length);
      }
  }
  else
  {
      float factor = n_len/def_length;
      for(int i=0;i<cur->pho_lengths.size();i++)
      {
          float length = cur->pho_lengths[i];
          printf("compress %f\n",length);  
          length *= factor;
          out_lengths.push_back(length);
      }
  }
  
  float accu = n_start;
  cur->pho_output.push_back(accu);
  for(int i=0;i<out_lengths.size();i++)
  {
	  accu += out_lengths[i];
      cur->pho_output.push_back(accu);
  }
  
  int count1 = cur->pho_times.size();
  int count2 = cur->pho_output.size();
  
  if(mbrolasing)  
  {
	printf("------------------------------\n");
	for(int i=0;i<cur->pho_codes.size()-1;i++)	//emit mbrola/festival commands
	{
		//,,,cur->pho_output[i]);
		//float a = out_lengths[i];
		//out_lengths[i+1]
		std::string left = cur->pho_codes[i];
		std::string right = cur->pho_codes[i+1];
		PhoInfo* diph = synth->lookupDiphone(left+"-"+right,cur->pho_output[i],cur->pho_output[i+1]);
		if(diph)
		{
			float start = cur->pho_output[i];
			float mid = start + out_lengths[i];
			float end = mid + out_lengths[i+1];
			printf("TODO emit: %s-%s %f,%f,%f\n",left.c_str(),right.c_str(),start,mid,end);
			
			float x[3]={start,mid,end};
			assert(diph->alignment.size()==3);
			synth->addUnit("", 3, x, diph->alignment.data());
		}
		else
		{
			printf("diphone %s-%s not found\n",left.c_str(),right.c_str());
		}
	}
	// diphone nphone interface needed
        
  }
  else
  {
      auto valid = cur->params.count("FileName");
  
      if (valid && n_len > 0 && count1==count2) {
		LOG_MSG("emit: "<<count2);
		synth->addUnit(cur->params["FileName"], count2, cur->pho_output.data(), cur->pho_times.data());
  
      }
  }
  

}



bool LyricTimelineESPEAK::isValid(){
    return true;
}

void LyricTimelineESPEAK::outputPho(IUnisyn* synth)
{ 
	
    //first query output
    for(int i=0;i<points.size();i++)
    {
         auto p = points[i];
         auto fileName = p->params["FileName"];
         
         
         if(mbrolasing)
         {
			 std::ifstream infile(fileName);
			 std::string line;
			 int j=0;
			 while (std::getline(infile, line)) {
					std::string key = pho(j);
					p->params[key]=line;
					printf("MBROLASING %i <%s>\n",j,line.c_str());
					j++;
			 }
		 }
		 else
		 {
			for(int j=0;j<256;j++)
			{
				std::string value = synth->getPhoLine(fileName,j);
				if(value.length()==0) break;
				std::string key = pho(j);
				p->params[key]=value;
			}
			float length = synth->getLengthForUnit(fileName);
			if(length>0) p->paramsf["Length"]=length;
         }
         
         
         
         
    }
    printf("LyricTimeline::outputPho()\n");
    LyricTimeline::outputPho(synth);
    
}


}



