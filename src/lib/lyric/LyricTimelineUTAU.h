#ifndef SINSY_LYRIC_TIMELINE_UTAU_H_
#define SINSY_LYRIC_TIMELINE_UTAU_H_

#include "LyricTimeline.h"
#include "UtauDB.h"

namespace sinsy
{

class LyricTimelineUTAU : public LyricTimeline
{ 
    vconnect::UtauDB *utaudb=nullptr;
  public:
    LyricTimelineUTAU(const std::string& voicepath,const std::string& codec);
    virtual bool isValid(); 
    virtual void fix();
    virtual void emit(lyricPoint* p0,lyricPoint* p1,IUnisyn* synth);
};



};

#endif
