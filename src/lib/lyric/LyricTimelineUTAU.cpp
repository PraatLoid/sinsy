#include "LyricTimelineUTAU.h"
#include "util_log.h"

namespace sinsy {

ILyricTimeline* newTimelineUTAU(std::string voicepath,std::string codec) { return new LyricTimelineUTAU(voicepath,codec); }

LyricTimelineUTAU::LyricTimelineUTAU(const std::string& voicepath,const std::string& codec)
{
     utaudb = new vconnect::UtauDB(voicepath, codec);
}

static float oto_preutter(lyricPoint* p)
{
    if(p->paramsf.count("PreUtterance"))
        return p->paramsf["PreUtterance"];
    return 0;
}
static float oto_overlap(lyricPoint* p)
{
    if(p->paramsf.count("VoiceOverlap"))
        return p->paramsf["VoiceOverlap"];
    return 0;
}

void LyricTimelineUTAU::fix()
{
    for(int i=0;i<points.size();i++)
    {
         auto p = points[i];
         vconnect::UtauParameter params;
         
         bool sokuon=false;
         
         if(p->lyric.size()>2 && p->lyric[0]==p->lyric[1])
         {
               char cons = p->lyric[0];
               if(cons == 'm' || cons == 'n' || cons == 'N' || cons == 'p' || cons == 'b' || cons == 'd' || cons=='t' || cons=='k' || cons == 'g' || cons=='l' || cons=='r') sokuon=true;
         }
         
         std::string lyric2 = p->lyric;
         if(sokuon)
         {
             lyric2.erase(0, 1);
             p->params["Sokuon"] = lyric2;
         }
         
         int found = utaudb->getParams(params, lyric2, p->midinotes[0]);
         if(found) 
         {
               p->paramsf["PreUtterance"] = 0.001 * params.msPreUtterance; 
               p->paramsf["VoiceOverlap"] = 0.001 * params.msVoiceOverlap; 
               p->paramsf["FixedLength"]  = 0.001 * params.msFixedLength;
               p->paramsf["LeftBlank"]    = 0.001 * params.msLeftBlank;
               p->paramsf["RightBlank"]   = 0.001 * params.msRightBlank;
               if(params.fileName.size()>0)
                    p->params["FileName"]      = params.fileName;
         }
         else
         {
            ERR_MSG("utau not found "<<p->lyric);
         }
    }
}

//mostly working correctly, Sokuon not yet implemented
void LyricTimelineUTAU::emit(lyricPoint* cur,lyricPoint* next,IUnisyn* synth)
{
  float n_start = cur->times[0];
  float n_end =  cur->times.back();

  if (next) {
     n_end -= oto_preutter(next);
     n_end += oto_overlap(next);
  }
  //TODO: validate oto ini entries
  n_start -= oto_preutter(cur);
  float n_len = n_end - n_start;
  auto valid = cur->params.count("FileName");
  
  if (valid && n_len > 0) {
    float l_fixed = cur->paramsf["FixedLength"];
    float l_blank = cur->paramsf["LeftBlank"];
    float r_blank = cur->paramsf["RightBlank"];
    std::string fileName = cur->params["FileName"];
    float u_len = synth->getLengthForUnit(fileName);
    if (n_len > l_fixed) {
      float ratio = n_len / l_fixed;
      float a[3] = {n_start, n_start + l_fixed, n_end};
      float b[3] = {l_blank, l_blank + l_fixed, u_len-r_blank};
      synth->addUnit(fileName, 3, a, b);
    } else {
      float a[2] = {n_start, n_start + n_len};
      float b[2] = {l_blank, l_blank + n_len};
      synth->addUnit(fileName, 2, a, b);
    }
  }

}



bool LyricTimelineUTAU::isValid(){
    return (utaudb->size()>0);
}


}



