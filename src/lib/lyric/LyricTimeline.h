#ifndef SINSY_LYRIC_TIMELINE_H_
#define SINSY_LYRIC_TIMELINE_H_

#include "sinsy.h"
#include "lyric.h"
#include <map>



namespace sinsy
{

struct lyricPoint
{
    std::string lyric;
    std::string dynamics;
    std::vector<float> times;
    std::vector<int> midinotes;
    
    std::map<std::string,std::string> params;
    std::map<std::string,float> paramsf;
    
    std::vector<float> pho_times;
    std::vector<float> pho_lengths;
    std::vector<std::string> pho_codes;
    std::vector<std::string> pho_types;
    std::vector<float> pho_output;
    
    
};

class LyricTimeline : public ILyricTimeline
{ 
  protected:
    std::vector<lyricPoint*> points;
    bool error=false;
    virtual void emit(lyricPoint* p0,lyricPoint* p1,IUnisyn* synth)=0;
  public:
    virtual bool isValid();
    virtual void addLyric(std::string lyric,float start,float length,int midinote,std::string dynamics="");
    virtual void fix();
    virtual void outputPho(IUnisyn* synth);
    virtual int getEventCount();
    virtual std::string getLyric(int index);
    virtual int getPitch(int index,bool maxElement);
    virtual void setParam(int index,std::string key,std::string value);
    virtual void setParam(int index,std::string key,float value);
    virtual float getStart(int index);
    virtual float getEnd(int index);
    
};

};

#endif


//non utau format [preutter,overlap?,outputTimes,outputCmd]
