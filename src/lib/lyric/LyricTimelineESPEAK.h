#ifndef SINSY_LYRIC_TIMELINE_ESPEAK_H_
#define SINSY_LYRIC_TIMELINE_ESPEAK_H_

#include "LyricTimeline.h"

namespace sinsy
{

//when eSpeak is used a a frontend -> backends can be diphone synth linke MBROLA or Festival
class LyricTimelineESPEAK : public LyricTimeline
{ 
  public:
    LyricTimelineESPEAK();
    virtual bool isValid(); 
    virtual void fix();
    virtual void emit(lyricPoint* p0,lyricPoint* p1,IUnisyn* synth);
    virtual void outputPho(IUnisyn* synth);
  private:
    bool mbrolasing=false;//FIXME: pass in constructor
};



};

#endif
