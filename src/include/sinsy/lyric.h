#ifndef SINSY_LYRIC_H_
#define SINSY_LYRIC_H_

#include <sekai/SynthInterface.h>

namespace sinsy
{
    


class ILyricTimeline
{
   public:
    virtual bool isValid()=0;
    virtual void addLyric(std::string lyric,float start,float length,int midinum,std::string dynamics="")=0;
    virtual void fix()=0;
    virtual void outputPho(IUnisyn* synth)=0;
    virtual int getEventCount()=0;
    virtual std::string getLyric(int index)=0;
    virtual int getPitch(int index,bool maxElement)=0;
    virtual void setParam(int index,std::string key,std::string value)=0;
    virtual void setParam(int index,std::string key,float value)=0;
    virtual float getStart(int index)=0;
    virtual float getEnd(int index)=0;
};

ILyricTimeline* newTimelineESPEAK();
ILyricTimeline* newTimelineUTAU(std::string voicepath,std::string codec);

}

#endif
